import javax.management.MBeanServer;
import javax.management.ObjectName;
import java.lang.management.ManagementFactory;

public class Agent {
    public static void main(String[] args) {
        try{
            MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();

            ObjectName name = new ObjectName("FOO:name=bruno");

            brunoBean mbean = new brunoBean();
            SomeBeanGui a = new SomeBeanGui();

            mbean.gui = a;

            mbs.registerMBean(mbean,name);
            System.out.println("czekam na zgloszenie");
            Thread.sleep(Long.MAX_VALUE);


        }catch (Exception e){
            System.out.println(e);
        }


    }
}
